package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TechGlobalLoginFormPage extends TechGlobalBasePage {
    public TechGlobalLoginFormPage() {
        super();
    }

    @FindBy(id = "main_heading")
    public WebElement mainHeading;


    @FindBy(id = "sub_heading")
    public WebElement resetPasswordHeading;

    @FindBy(css = "label[for='username']")
    public WebElement usernameLabel;

    @FindBy(css = "label[for='email']")
    public WebElement instructionsToResetPassword;

    @FindBy(id = "username")
    public WebElement usernameInputField;

    @FindBy(id = "confirmation_message")
    public WebElement validEmailForgotPasswordConfirmationMessage;

    @FindBy(id = "error_message")
    public WebElement invalidEmailOrPasswordMessage;

    @FindBy(id = "email")
    public WebElement forgotPasswordInputField;

    @FindBy(css = "label[for='password']")
    public WebElement passwordLabel;

    @FindBy(id = "password")
    public WebElement passwordInputField;

    @FindBy(id = "login_btn")
    public WebElement loginButton;

    @FindBy(id = "submit")
    public WebElement submitForgotPasswordButton;

    @FindBy(id = "forgot-password")
    public WebElement forgotPasswordButton;

    @FindBy(id = "logout")
    public WebElement logoutButton;

    @FindBy(id = "success_lgn")
    public WebElement loggedInMessage;

    @FindBy(id = "forgot-password")
    public WebElement forgotPasswordLink;
}
