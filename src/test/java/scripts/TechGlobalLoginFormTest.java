package scripts;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.TechGlobalFrontendTestingHomePage;
import pages.TechGlobalLoginFormPage;

public class TechGlobalLoginFormTest extends TechGlobalBase {
    @BeforeMethod
    public void setPage() {
        techGlobalFrontendTestingHomePage = new TechGlobalFrontendTestingHomePage();
        techGlobalLoginFormPage = new TechGlobalLoginFormPage();
        techGlobalFrontendTestingHomePage.headerDropdown.click();
        techGlobalFrontendTestingHomePage.headerDropdownOptions.get(0).click();
        techGlobalFrontendTestingHomePage.clickOnCard(15);
    }

    @Test(priority = 1, description = "Validate Login Form card")
    public void validateLoginFormCard() {
        // Validate heading visibility and text
        Assert.assertTrue(techGlobalLoginFormPage.mainHeading.isDisplayed());
        Assert.assertEquals(techGlobalLoginFormPage.mainHeading.getText(), "Login Form");

        // Validate username label visibility and text
        Assert.assertTrue(techGlobalLoginFormPage.usernameLabel.isDisplayed());
        Assert.assertEquals(techGlobalLoginFormPage.usernameLabel.getText(), "Please enter your username");
        Assert.assertTrue(techGlobalLoginFormPage.usernameInputField.isDisplayed());

        // Validate password label visibility and text
        Assert.assertTrue(techGlobalLoginFormPage.passwordLabel.isDisplayed());
        Assert.assertEquals(techGlobalLoginFormPage.passwordLabel.getText(), "Please enter your password");
        Assert.assertTrue(techGlobalLoginFormPage.passwordInputField.isDisplayed());

        // Validate login button visibility and text
        Assert.assertTrue(techGlobalLoginFormPage.loginButton.isDisplayed());
        Assert.assertEquals(techGlobalLoginFormPage.loginButton.getText(), "LOGIN");

        // Validate forgot password link visibility and text
        Assert.assertTrue(techGlobalLoginFormPage.forgotPasswordLink.isDisplayed());
        Assert.assertEquals(techGlobalLoginFormPage.forgotPasswordLink.getText(), "Forgot Password?");
    }

    @Test(priority = 2, description = "Validate Login Form card Logout button")
    public void validateLoginFormCardLogoutButton() {
        techGlobalLoginFormPage.usernameInputField.sendKeys("TechGlobal");
        techGlobalLoginFormPage.passwordInputField.sendKeys("Test1234");
        techGlobalLoginFormPage.loginButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.loggedInMessage.getText(), "You are logged in");
        Assert.assertTrue(techGlobalLoginFormPage.logoutButton.isDisplayed());
    }

    @Test(priority = 3, description = "Validate Login Form card Login & Logout")
    public void validateLoginFormCardLoginLogout() {
        techGlobalLoginFormPage.usernameInputField.sendKeys("TechGlobal");
        techGlobalLoginFormPage.passwordInputField.sendKeys("Test1234");
        techGlobalLoginFormPage.loginButton.click();
        techGlobalLoginFormPage.logoutButton.click();
        Assert.assertTrue(techGlobalLoginFormPage.mainHeading.isDisplayed());
    }

    @Test(priority = 4, description = "Validate Login Form card Forgot Password")
    public void validateLoginFormCardForgotPassword() {
        techGlobalLoginFormPage.forgotPasswordButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.resetPasswordHeading.getText(), "Reset Password");
        Assert.assertEquals(techGlobalLoginFormPage.instructionsToResetPassword.getText(), "Enter your email address and we'll send you a link to reset your password.");
        Assert.assertTrue(techGlobalLoginFormPage.forgotPasswordInputField.isDisplayed());
        Assert.assertTrue(techGlobalLoginFormPage.submitForgotPasswordButton.isDisplayed());
    }

    @Test(priority = 5, description = "Validate Login Form card Forgot Password with valid info")
    public void validateLoginFormCardForgotPasswordWithValidInfo() {
        techGlobalLoginFormPage.forgotPasswordButton.click();
        techGlobalLoginFormPage.forgotPasswordInputField.sendKeys("TechGlobal@aol.com");
        techGlobalLoginFormPage.submitForgotPasswordButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.validEmailForgotPasswordConfirmationMessage.getText(), "A link to reset your password has been sent to your email address.");
    }

    @Test(priority = 6, description = "Validate Login Form card Invalid Username")
    public void validateLoginFormCardInvalidUsername() {
        techGlobalLoginFormPage.usernameInputField.sendKeys("john");
        techGlobalLoginFormPage.passwordInputField.sendKeys("Test1234");
        techGlobalLoginFormPage.loginButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.invalidEmailOrPasswordMessage.getText(), "Invalid Username entered!");
    }

    @Test(priority = 7, description = "Validate Login Form card Invalid Password")
    public void validateLoginFormCardInvalidPassword() {
        techGlobalLoginFormPage.usernameInputField.sendKeys("TechGlobal");
        techGlobalLoginFormPage.passwordInputField.sendKeys("1234");
        techGlobalLoginFormPage.loginButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.invalidEmailOrPasswordMessage.getText(), "Invalid Password entered!");
    }

    @Test(priority = 8, description = "Validate Login Form card Invalid Username and Password")
    public void validateLoginFormCardInvalidUsernameAndPassword() {
        techGlobalLoginFormPage.usernameInputField.sendKeys("john");
        techGlobalLoginFormPage.passwordInputField.sendKeys("1234");
        techGlobalLoginFormPage.loginButton.click();
        Assert.assertEquals(techGlobalLoginFormPage.invalidEmailOrPasswordMessage.getText(), "Invalid Username entered!");
    }




}
